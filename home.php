<?php include 'db.php';
ob_start(); 

if (empty($_SESSION['username'])) {
    header("Location: index.php");
}
	$id=$_SESSION['user_id'];
	$username=$_SESSION['username'];

?>
<!DOCTYPE html>
<html>
<head><style type="text/css" href="front/css/main.css"></style>
	<title> </title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="home.php">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
       <a class="nav-link" href="post.php">Make a Post</a>
      </li>
      <li class="nav-item">
       <a class="nav-link" href="logout.php">Logout</a>
      </li>
      <li class="nav-item">
        <?php echo "<a  class='nav-link' href='profile.php?id=$id'>$username (Profili)</a>"?>
      </li>
    </ul>

  </div> <form class="form-inline" action="search.php" method="post">
    <input class="form-control mr-sm-2" name="submit" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
	<div class="container-fluid">

		<?php 
          $query="SELECT * FROM posts  WHERE  user_id=$id";
            
                 $show_posts=mysqli_query($connection,$query);

                 
                $count=mysqli_num_rows($show_posts);
                 if ($count >0) {
                   
                 while ($row=mysqli_fetch_assoc($show_posts)) {
                         
                    
                     $post_id=$row['id'];
                     $post_content=$row['content'];
                     $post_image=$row['featured'];

                     echo "<div class='d-flex justify-content-center'>";
                     echo "<div class='card' style='width: 24rem;'>";
                     if ($post_image!="images/") {
                     echo "<img src='$post_image' class='card-img-top'>";
                     }
                     echo "<div class='card-body'>";
                     echo "<p class='card-title' style='border 1px solid'>$post_content</p>";
                     echo "<a href='edit_post.php?post_id=$post_id&&user_id=$id'>edit<a/>";
                     echo "<br/>";
                     echo "<a href='delete_post.php?post_id=$post_id&&user_id=$id'>delete<a/>";
                     echo "</div>"; 
                     echo "</div>";
                     echo "</div>";
                     

}
}
                 ?>
         
</div>


</body>
</html>