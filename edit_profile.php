<?php include 'db.php';
ob_start(); 
if (empty($_SESSION['username'])) {
    header("Location: index.php");
}


	$id=$_SESSION['user_id'];

?>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="home.php">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
       <a class="nav-link" href="logout.php">Logout</a>
      </li>
      <li class="nav-item">
    <?php echo "<a class='nav-link' href='profile.php?id=$id'>Profile</a>"; ?>
      </li>
    </ul>

  </div> <form class="form-inline" action="search.php" method="post">
    <input class="form-control mr-sm-2" name="submit" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
</nav>
<?php 
      $query="SELECT * FROM profiles  WHERE  user_id=$id";
            
                 $show_profile=mysqli_query($connection,$query);

                 
              
                   
                $count=mysqli_num_rows($show_profile);
                 if ($count >0) {
                 while ($row=mysqli_fetch_assoc($show_profile)) {     
                     $profile_id=$row['id'];
                     $profile_firstname=$row['firstname'];
                     $profile_lastname=$row['lastname'];
                     $profile_birthday=$row['birthday'];
                     $profile_country=$row['country'];
                     $profile_city=$row['city'];
                     $profile_image=$row['image'];
                 }}
if (isset($_POST['submit'])) {
	$first_name=escape($_POST['first_name']);
	$last_name=escape($_POST['last_name']);
	$birthday=escape($_POST['birthday']);
	$country=escape($_POST['country']);
	$city=escape($_POST['city']);

    $image=escape($_FILES['user_image']['name']);
    if (empty($image)) {
    	
     $query="UPDATE profiles SET ";
    $query .="firstname ='{$first_name}', "; 
    $query .="lastname='{$last_name}', ";
    $query .="birthday= '{$birthday}', ";
    $query .="country ='{$country}', ";
    $query .="city='{$city}', ";
    $query .="image='{$profile_image}' ";
    $query .=" WHERE user_id = {$id}";
    }else{
  $path = "userImages/";
    $path = $path . basename( $_FILES['user_image']['name']);
    if(move_uploaded_file($_FILES['user_image']['tmp_name'], $path)) {
      echo "The file ".  basename( $_FILES['user_image']['name']). 
      " has been uploaded";
    } else{
        echo "There was an error uploading the file, please try again!";
    }

    
     $query="UPDATE profiles SET ";
    $query .="firstname ='{$first_name}', "; 
    $query .="lastname='{$last_name}', ";
    $query .="birthday= '{$birthday}', ";
    $query .="country ='{$country}', ";
    $query .="city='{$city}', ";
    $query .="image ='{$path}' ";
    $query .=" WHERE user_id = {$id}";
   
     
}
     $update_profile_query=mysqli_query($connection,$query);

    header("Location: profile.php?id=$id");
}




 ?>
<form action="edit_profile.php" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="title">User Image</label>
		<input type="file"  name="user_image" >
	</div>
	<div class="form-group">
		<label for="title">first name</label>
	<input type="text" name="first_name" value="<?php echo $profile_firstname ?>">
	<div class="form-group">
		<label for="title">last name</label>
	<input type="text" name="last_name" value="<?php echo $profile_lastname ?>">
	<div class="form-group">
		<label for="title">birthday</label>
	<input type="date" name="birthday" value="<?php echo $profile_birthday ?>">
	<div class="form-group">
		<label for="title">country</label>
	<input type="text" name="country" value="<?php echo $profile_country?>">
	<div class="form-group">
		<label for="title">city</label>
	<input type="text" name="city" value="<?php echo $profile_city ?>">
</div>
<div class="form-group">
                <div class="text-center">
                <button class="btn btn-success" name="submit">Submit</button> 
            </div>
</form>